/*global define*/
define([
    "dojo/_base/declare",
    "rdforms/template/Item"
], function(declare, Item) {

    /**
     * Same functionality as an Item, but separate class to make switching on type possible.
     */
    return declare(Item, {
    });
});